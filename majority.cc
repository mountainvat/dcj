#include <message.h>
#include <cassert>
#include <cstdio>
#include <string.h>
#include <map>

#include "majority.h"

int main() {
  long size = (GetN() - 1) / NumberOfNodes() + 1;
	//printf("Node: %d, size: %ld\n", MyNodeId(), size);
	long counts[32];
	memset(counts, 0, sizeof(counts));
  for (long c = 0 ; c < size; c++) {
		long i = c * NumberOfNodes() + MyNodeId();
		if (i >= GetN())
			break;
		
		long vote = GetVote(i);
		//printf("%ld ", vote);
		int bits = 0;
		while (bits < 32) {
			bool set = vote & 1 > 0;
			counts[bits] += (set ? 1 : -1);
			//printf("%d: %ld ", bits, counts[bits]);
			vote = vote >> 1;
			bits++;
		}
	}    
	//printf("\n");
	long long candidate = 0;
    for (int bits = 31; bits >= 0; bits--) {
			//printf("%ld ", counts[bits]);
      candidate = (candidate << 1) + (counts[bits] > 0 ? 1 : 0);
			//printf("%d", candidate);
    }

	//printf("#%d: %ld\n", MyNodeId(), candidate);
	// Send all candidate.
	for (int i = 0; i < NumberOfNodes(); i++) {
		PutLL(i, candidate);
	  Send(i);
	}
	
	// Receive all's candidates.
		long candidates[NumberOfNodes()];
		std::map<long, long> dict;
		for (int i = 0; i < NumberOfNodes(); i++) {
			Receive(i);
			candidates[i] = GetLL(i);
			dict[candidates[i]] = 0;
			//printf("R:%ld\n", candidates[i]);
		}

  // Count again.
  for (long c = 0 ; c < size; c++) {
    long i = c * NumberOfNodes() + MyNodeId();
    if (i >= GetN())
      break;
    
    long vote = GetVote(i);
		if (dict.find(vote) == dict.end())
			continue;
	
		dict[vote] = dict[vote] + 1;	
	}

	// send back 0 counts.
	PutLL(0, dict.size());
  	for (std::map<long, long>::iterator it=dict.begin(); it!=dict.end(); ++it) {
		//printf("%ld: %ld\n", it->first, it->second);
		PutLL(0, it->first);
		PutLL(0, it->second);
	}
	Send(0);
	
	if (MyNodeId() == 0) {
		std::map<long, long> dict2;
		for (int i = 0; i < NumberOfNodes(); i++)
			dict2[candidates[i]] = 0;
		long maxCount = 0;
		long maxCandidate = 0;
		for (int i = 0; i < NumberOfNodes(); i++) {
			Receive(i);
			long long size2 = GetLL(i);
			for (int j = 0; j < size2; j++) {
				long long cand = GetLL(i);
				long long cnt = GetLL(i);
				//printf("cand %lld: %lld", cand, cnt);
				dict2[cand] = dict2[cand] + cnt;
				if (dict2[cand] > maxCount) {
					maxCount = dict2[cand];
					maxCandidate = cand;
				}
			}			
		}
		//printf("%ld: %ld (%lld)", maxCandidate, maxCount, GetN()/2);
		if (maxCount > GetN()/2)
			printf("%ld\n", maxCandidate);
		else
			printf("NO WINNER\n");
	}
	return 0;
}

