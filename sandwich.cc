#include <message.h>
#include <cassert>
#include <cstdio>

#include "sandwich.h"

int main() {
  //printf("Number of nodes: %d\n", NumberOfNodes());
  int lotSize = (GetN() - 1) / NumberOfNodes() + 1;
  if (MyNodeId() >= GetN()) {
  	lotSize = 0;
	}
  long long start = MyNodeId() * lotSize;
  long long sum = 0, minL = 0, maxL = 0, minM = 0, runningMin = 0;
  for (long long i = start; i < start + lotSize && i < GetN(); i++) {
    long long taste = GetTaste(i);
		sum += taste;
    if (sum > maxL)
      maxL = sum;
    if (sum < minL)
      minL = sum;
 		runningMin += taste;
    if (runningMin < minM)
  		minM = runningMin;
		if (runningMin > 0)
			runningMin = 0;   
  }
	long long minR = sum - maxL;
  PutLL(0, sum);
  PutLL(0, minL);
  PutLL(0, minM);
  PutLL(0, minR);
  Send(0);
	//printf("%lld %lld %lld %lld\n", minL, minM, minR, sum);
	if (MyNodeId() == 0) {
		long long grandSum = 0;
		long long grandMin = 0;
		long long grandMinM = 0;
		long long runningSum = 0;
  	for (int i = 0; i < NumberOfNodes(); i++) {
			Receive(i);
		  long long iSum = GetLL(i);
      long long iMinL = GetLL(i);
 			long long iMinM = GetLL(i);
			long long iMinR = GetLL(i);
			if (iMinM < grandMinM)
				grandMinM = iMinM;
      grandSum += iSum;
			long long test = runningSum + iMinL;
      if (test < grandMin)
  			grandMin = test;
      runningSum += iSum;
		  if (runningSum < grandMin)
				grandMin = runningSum;
			if (runningSum > iMinR)
				runningSum = iMinR;
    }
	if (runningSum < grandMin)
		grandMin = runningSum;
	  if (grandMin > grandMinM)
			grandMin = grandMinM;
		printf("%lld\n", grandSum - grandMin);
  }
  //printf("Node %d: %lld", MyNodeId(), sum);
  return 0;
}

