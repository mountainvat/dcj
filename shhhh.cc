#include <message.h>
#include <cassert>
#include <cstdio>
#include <time.h>       /* time */
#include <set>
#include "shhhh.h"
#include <cstdlib>

bool isAStop(long index) {
	if (index == 0 || index == 1)
		return true;
	if (index >= GetN())
		return false;

	long size = (GetN()-1)/NumberOfNodes() + 1;
	return index % size == 0;
}

int main() {
	//srand (time(NULL));
        long seeds[NumberOfNodes()];
	//printf("node: %d\n", MyNodeId());
	if (MyNodeId() == 0) {
		seeds[0] = 0;
		std::set<long> seen;
		seen.insert(0);
		seen.insert(1);
		long count = 1;	
		for (int i = 0; i < NumberOfNodes()-1; i++) {
			while (true) {
				long index  = std::rand() % GetN();
				//printf("index: %ld", index);
				if (seen.find(index) == seen.end()) {
					seeds[count++] = index;
					seen.insert(index);
					break;
				}
			}
			if (seen.size() >= GetN()) break;
		}
		// send seeds out
		//printf("count: %ld\n", count);
		//for (int i = 0 ;  i < count; i++)
		//	printf("%ld ", seeds[i]);
		//printf("\n");
	 	for (int i = 0; i < NumberOfNodes(); i++) {
			PutLL(i, count);
			for (int j = 0; j < count; j++) {
				PutLL(i, seeds[j]);
			}
			Send(i);
		}
	}
	// get seeds.
	Receive(0);
	long count = GetLL(0);
	//printf("received count: %ld\n", count);
	std::set<long> stops;
	for (int i = 0; i < count; i++) {
		seeds[i] = GetLL(0);
		//printf("%ld ", seeds[i]);
		stops.insert(seeds[i]);
	}
	stops.insert(1);
	
	long start = MyNodeId() >= count ? -1 : seeds[MyNodeId()];
	//printf("start: %ld\n", start);
	long next = start == -1 ? -1 : GetLeftNeighbour(start);
	long dis = 1;
	if (start != -1) {
  		while (stops.find(next) == stops.end()) {
			//printf("next: %ld ", next);
			next = GetLeftNeighbour(next);
			dis++;
		}
	}
	PutLL(0, start);
	PutLL(0, next);
	PutLL(0, dis);
	Send(0);
	
	if (MyNodeId() == 0) {
		long starts[NumberOfNodes()];
		long nexts[NumberOfNodes()];
		long dists[NumberOfNodes()];
		for (int i = 0; i < NumberOfNodes(); i++) {
			Receive(i);
			long long iStart = GetLL(i);
			long long iNext = GetLL(i);
			long long iDis = GetLL(i);
			starts[i] = iStart;
			nexts[i] = iNext;
			dists[i] = iDis;		
		}
		start = 0;
		dis = 0;
		while (start != 1) {
			int i = 0;
			for (i = 0; i < NumberOfNodes(); i++) {
				if (starts[i] == start)
					break;
			}
			dis += dists[i];
			start = nexts[i];
		}
    long rightDis = GetN() - dis - 1;
    if (dis < rightDis)
      printf("LEFT %ld", dis);
    else if (dis > rightDis)
      printf("RIGHT %ld", rightDis);
    else
      printf("WHATEVER %ld", dis);

	}
	return 0;
}

